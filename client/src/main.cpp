#include <iostream>
#include <boost/asio.hpp>

using boost::asio::ip::udp;

int main(int argc, char* argv[]) try
{
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << " <resource_name>" << std::endl;
        return 1;
    }

    boost::asio::io_context io_context;
    udp::socket socket(io_context, udp::endpoint(udp::v4(), 0));

    udp::resolver resolver(io_context);
    udp::endpoint server_endpoint = *resolver.resolve(udp::v4(), "localhost", "2024").begin();

    std::string request = argv[1];
    socket.send_to(boost::asio::buffer(request), server_endpoint);

    char data[1024];
    udp::endpoint sender_endpoint;
    size_t length = socket.receive_from(boost::asio::buffer(data, 1024), sender_endpoint);

    std::string response(data, length);

    std::cout << response << std::endl;

    return 0;
}
catch (const std::exception& e)
{
    std::cerr << "Exception: " << e.what() << std::endl;
    return 1;
}