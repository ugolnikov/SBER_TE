#include <iostream>
#include <fstream>
#include <optional>
#include <regex>
#include <utility>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>


#undef FindResource //MinGW использует этот дефайн

class Storage
{
    std::string _fileName;

public:
    explicit Storage(std::string fileName)
        : _fileName(std::move(fileName)) {}

    std::optional<std::string> GetResource(const std::string& resourceName)
    {
        std::ifstream file;
        file.open(_fileName, std::ios::in);
        if (!file.is_open())
        {
            std::cerr << "Can't open resource file " << _fileName << std::endl;
            return {};
        }

        auto body = FindResource(resourceName, file);
        file.close();
        if (!body)
        {
            return {};
        }

        return {body};
    }

private:
    std::optional<std::string> FindResource(const std::string& resourceName, std::istream& input)
    {
        while (true)
        {
            auto [id, body] = ReadResource(input);
            if (id.empty())
            {
                //Конец файла. Ресурс не найден
                return {};
            }

            if (id == resourceName)
            {
                return {body};
            }
        }
    }

    std::pair<std::string, std::string> ReadResource(std::istream& input)
    {
        //Получаем строку из файла
        std::string line;
        if (!std::getline(input, line))
        {
            return {};
        }

        //Читаем части формата хранилища ресурсов
        std::vector<std::string> partitions;
        std::regex pattern("\\s{3}");
        std::sregex_token_iterator iter(line.begin(), line.end(), pattern, -1);
        std::sregex_token_iterator end;

        partitions.insert(partitions.begin(), iter, end);

        if (partitions.size() != 2)
        {
            return {};
        }

        return {partitions[0], partitions[1]};
    }
};


class WorkerGroup
{
public:
    void AddThread(std::thread&& thread)
    {
        _threads.emplace_back(std::move(thread));
    }


    void JoinAll()
    {
        for (auto& thread : _threads)
        {
            if (thread.joinable())
            {
                thread.join();
            }
        }
    }


    ~WorkerGroup()
    {
        JoinAll();
    }

private:
    std::vector<std::thread> _threads;
};


class MediaServer
{
    static const int IN_BUFFER_SIZE = 1024;
    static const int PARALLEL_WORKERS = 2;
    unsigned int _port;

    std::shared_ptr<boost::asio::ip::udp::socket> _socket;
    std::shared_ptr<Storage> _storage;
    std::shared_ptr<char> _input_buffer;

    std::mutex _send_mutex;
    std::mutex _receive_mutex;

public:
    MediaServer(unsigned int port, std::shared_ptr<Storage> storage)
        : _port(port)
        , _storage(std::move(storage))
    {
        _input_buffer = std::shared_ptr<char>(new char[IN_BUFFER_SIZE], [](const char* buffer){delete[] buffer;});
    }


    void Run()
    {
        try
        {
            using boost::asio::ip::udp;
            boost::asio::io_context io_context;
            _socket = std::make_shared<udp::socket>(io_context, udp::endpoint(udp::v4(), _port));
            std::cout << "Server start listening on port " << _port << "..." << std::endl;

            WorkerGroup workers;
            for (int i = 0; i < PARALLEL_WORKERS; i++)
            {
                workers.AddThread(std::thread([&](){
                    worker(io_context);
                }));
            }

            workers.JoinAll();
        }
        catch (const boost::system::system_error& e)
        {
            std::cerr << "Boost.Asio system error: " << e.what() << ", code: " << e.code() << std::endl;
        }
        catch (const std::exception& e)
        {
            std::cerr << "Exception :" << e.what() << std::endl;
        }
    }

private:
    void worker(boost::asio::io_context& io_context)
    {
        while(true)
        {
            try
            {
                using boost::asio::ip::udp;

                udp::endpoint client_endpoint;
                boost::system::error_code error;

                size_t bytes_received = syncReceiveFrom(client_endpoint, error);

                if (error) {
                    std::cerr << "Receive error: " << error.message() << std::endl;
                }

                handleRequest(error, bytes_received, client_endpoint);

                io_context.run();
            }
            catch (const boost::system::system_error& e)
            {
                std::cerr << "Boost.Asio system error: " << e.what() << ", code: " << e.code() << std::endl;
            }
            catch (const std::exception& e)
            {
                std::cerr << "Exception :" << e.what() << std::endl;
            }
        }
    }


    void handleRequest(const boost::system::error_code& error, std::size_t bytes_received
                       , const boost::asio::ip::udp::endpoint& client_endpoint)
    {
        try
        {
            if (error)
            {
                std::cerr << error.message() << std::endl;
            }

            std::string resourceId(_input_buffer.get(), bytes_received);

            std::cout << "Received message: " << resourceId << std::endl;

            auto resource = _storage->GetResource(resourceId);
            if (!resource)
            {
                std::cerr<< __PRETTY_FUNCTION__ << " Resource " << resourceId << " was not found" << std::endl;
                sendError("Resource was not found", client_endpoint);
                return;
            }

            sendResource(resource.value(), client_endpoint);
        }
        catch (const boost::system::system_error& e)
        {
            std::cerr << "Boost.Asio system error: " << e.what() << ", code: " << e.code() << std::endl;
            sendError("Server internal error", client_endpoint);
        }
        catch (const std::exception& e)
        {
            std::cerr << "Exception :" << e.what() << std::endl;
            sendError("Server internal error", client_endpoint);
        }
    }


    size_t syncReceiveFrom(boost::asio::ip::udp::endpoint& client_endpoint, boost::system::error_code& error)
    {
        std::lock_guard<std::mutex> l(_receive_mutex);
        return _socket->receive_from(boost::asio::buffer(_input_buffer.get(), IN_BUFFER_SIZE), client_endpoint, 0, error);
    }


    void sendResource(const std::string& resource, const boost::asio::ip::udp::endpoint& client_endpoint)
    {
        std::lock_guard<std::mutex> l(_send_mutex);

        std::string response;
        response = "-BEGIN-\n" + resource + "\n-END-";

        std::cout << "Sending: " << response << std::endl;

        _socket->send_to(boost::asio::buffer(response), client_endpoint);
    }


    void sendError(const std::string& error, const boost::asio::ip::udp::endpoint& client_endpoint)
    {
        std::lock_guard<std::mutex> l(_send_mutex);

        std::string response;
        response = "-ERROR-\n" + error + "\n-END-";

        _socket->send_to(boost::asio::buffer(response), client_endpoint);
    }
};


int main(int argc, char* argv[])
{
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << " <port>" << std::endl;
        return 1;
    }

    unsigned int port = std::stoi(std::string(argv[1]));

    auto storage = std::make_shared<Storage>("resources.csv");
    MediaServer server(port, storage);
    server.Run();

    std::cout << "..Exit" << std::endl;
    return 0;
}